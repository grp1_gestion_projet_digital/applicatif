"""
Ce script permet de nettoyer tout fichier contenant des données de l'utilisateur.
Il est lancé par le script VBA afin d'assurer la protection des données de nos utilisateurs.
"""
import os
import data_cleaning
import logging

def clear_predict_txt(src_path: str):
  if os.path.exists(os.path.join(src_path, 'data', 'predict.txt')):
    os.remove(os.path.join(src_path, 'data', 'predict.txt'))

def clear_user_data_csv(src_path: str):
  if os.path.exists(os.path.join(src_path, 'data', 'UserData.csv')):
    os.remove(os.path.join(src_path, 'data', 'UserData.csv'))

if __name__ == "__main__":
  src_path = data_cleaning.nth_parent_folder(2)
  logging.basicConfig(filename=os.path.join(src_path, 'log', 'user.log'), level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
  logging.info('------------ start clearing_user_data.py ------------')
  clear_predict_txt(src_path)
  logging.info('predict.txt cleared')
  clear_user_data_csv(src_path)
  logging.info('UserData.csv cleared')
  logging.info('------------ end clearing_user_data.py ------------')