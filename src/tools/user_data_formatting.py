"""
Ce script vise à importer formatter les données du csv (UserData.csv) de VBA pour 
qu'elles soient utilisables par le modèle
"""

# 0. Imports
import os
import logging
import pandas as pd

# 1. Function definition
# Fonction pour récupérer le chemin du dossier parent
def nth_parent_folder(n: int):
    """
    Retourne le chemin du n-ieme parent dans du fichier

    Args:
        n (int): _description_

    Returns:
        str: chemin absolu vers n-ième dossier parent
    """
    # get the path of the current file
    parent_path = os.path.realpath(__file__)
    
    # go up n times in the folder hierarchy
    for i in range(n):
        parent_path = os.path.dirname(parent_path)
    
    return parent_path

def translate_french_to_english(userdata: pd.DataFrame):
    """translate_french_to_english translates the french answers into english

    Args:
        userdata (pd.DataFrame): df with french answers

    Returns:
        pd.DataFrame: df with english answers
    """
    # dict to translate the french answers into english - to be polished ...................................................................................................
    dict_fr_en = {"Oui" : "yes",
                  "Non" : "no",
                  "Homme" : "male",
                  "Femme" : "female",
                  "Autre" : "non_binary",
                  "Je ne sais pas" : "dont_know",
                  "Rarement" : "rarely",
                  "Souvent" : "often",
                  "Parfois" : "sometimes",
                  "Jamais" : "never",
                  "Tres facile" : "very_easy",
                  "Un peu facile" : "somewhat_easy",
                  "Un peu difficile" : "somewhat_difficult",
                  "Tres difficile" : "very_difficult",
                  "Certains d'entre eux" : "some_of_them",
                  "Peut-etre" : "maybe",
                  "Pas sur" : "not_sure"}
    
    userdata = userdata.replace(dict_fr_en)
    
    return userdata

def format_user_data(userdata: pd.DataFrame):
    """Returns the formatted user data with the whole list of dummyvariables

    Args:
        userdata (pd.DataFrame): user data coming from csv and VBA
    """
    # traduire les réponses en anglais
    userdata = translate_french_to_english(userdata)
    
    userdata.columns = userdata.columns.str.lower()
    
    # remplir les dummys manquantes avec des 0
    # remplir les dummys manquantes avec des 0
    entire_variables = [['anonymity_dont_know', 'anonymity_no', 'anonymity_yes'], 
                        ['benefits_dont_know', 'benefits_no', 'benefits_yes'], 
                        ['care_options_no', 'care_options_not_sure', 'care_options_yes'], 
                        ['coworkers_no', 'coworkers_some_of_them', 'coworkers_yes'], 
                        ['family_history_yes', 'family_history_no'],
                        ['gender_female', 'gender_male', 'gender_non_binary'], 
                        ['leave_dont_know', 'leave_somewhat_difficult', 'leave_somewhat_easy', 'leave_very_difficult', 'leave_very_easy'],
                        ['mental_health_consequence_maybe', 'mental_health_consequence_no','mental_health_consequence_yes'], 
                        ['mental_health_interview_maybe', 'mental_health_interview_no', 'mental_health_interview_yes'],
                        ['mental_vs_physical_dont_know', 'mental_vs_physical_no','mental_vs_physical_yes'], 
                        ['obs_consequence_yes', 'obs_consequence_no'],
                        ['seek_help_dont_know', 'seek_help_no', 'seek_help_yes'],
                        ['wellness_program_dont_know', 'wellness_program_no', 'wellness_program_yes'], 
                        ['work_interfere_never', 'work_interfere_often', 'work_interfere_rarely',
                        'work_interfere_sometimes', 'work_interfere_unknown']]

    
    userdata = pd.get_dummies(userdata)

    for i in entire_variables:
        changes = 0  
        for col in i:
            if col not in userdata.columns:
                userdata[col] = 0
                changes += 1
        if changes == len(i):
            logging.warning(f"None of the columns {i} were found in the user data, all data will be 0")
            logging.warning(f"the model will use the first colinear column as answer (generally I don't know or smthg like that)")
            logging.info("It is possible that this info is not necessary to the model, in that case, ignore this warning")
    
    userdata['family_history'] = userdata['family_history_yes']
    userdata['obs_consequence'] = userdata['obs_consequence_yes']

    return(userdata)


# 2. Test fonctionnement général
if __name__ == "__main__":       
    print(nth_parent_folder(2))                                                     
    print(format_user_data(pd.DataFrame({"seek_help" : "Oui", "family_history" : "Non"})))
