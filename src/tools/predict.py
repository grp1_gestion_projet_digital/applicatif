"""
this script retrieves the user data, cleans it, applies the model stored in 
a pickle and returns the prediction as a txt file       
"""

# 0. imports
import os
import pickle
import logging
import pandas as pd

from sklearn.ensemble import AdaBoostClassifier

import user_data_formatting

## 0.1 setting paths
tool_path = user_data_formatting.nth_parent_folder(1)
src_path = user_data_formatting.nth_parent_folder(2)

## 0.2 setting logging
logging.basicConfig(filename=os.path.join(src_path, 'log', 'user.log'), level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
logging.info('------------ start predict.py ------------')

# 1. Opening pickle with a with open statement
# option rb permet de juste lire le fichier binaire
with open(os.path.join(tool_path, 'stored_model.pkl'), 'rb') as file: 
    dict_model = pickle.load(file) 
    logging.info('Model loaded from model.pkl')


# 2. user data retrieval
## If the VBA export works it used it as user data to predict
## Otherwise we input a dict just for testing purposes
## Try-except statement to check if the file exists and with open statement (like for the pickle)
## then we apply the data formatting function to fit the model
try:
    with open(os.path.join(src_path, 'data', 'UserData.csv')) as f :
        user_data = user_data = user_data_formatting.format_user_data(pd.read_csv(f))
        logging.info('User data loaded from UserData.csv and formatted')
        
except FileNotFoundError:    
    user_data = pd.DataFrame({'Timestamp': '2014-08-27 11:29:37', 'age': 44, 'Country': 'Peru', 'anonymity_no': 1, 
                              'anonymity_yes': 0, 'benefits_no' : 0, 'benefits_yes' : 1,'care_options_not_sure' : 0, 
                              'care_options_yes' : 1, 'coworkers_some_of_them' : 0, 'coworkers_yes' : 1, 'family_history' : 0, 
                              'gender_male' : 1, 'gender_non_binary' : 0, 'leave_somewhat_difficult' : 0, 'leave_somewhat_easy' : 0,
                              'leave_very_difficult' : 0, 'leave_very_easy' : 1,'mental_health_consequence_no' : 1, 'mental_health_consequence_yes' : 0,
                              'mental_health_interview_no' : 1, 'mental_health_interview_yes' : 0, 'mental_vs_physical_no' : 0, 'mental_vs_physical_yes' : 1, 
                              'obs_consequence' : 0, 'seek_help_no' : 0, 'seek_help_yes' : 1, 'wellness_program_no' : 0,'wellness_program_yes' : 1, 
                              'work_interfere_often' : 0, 'work_interfere_rarely' : 0,'work_interfere_sometimes' : 1, 'work_interfere_unknown' : 0}, 
                             index=[0])
    logging.warning("No user data found at all, using example dict instead")
    logging.warning("This is done for testing purposes and at the startup of the vba to have a smoother use after the first launch")
    logging.info('User data loaded from example dict and formatted')


## selection model variables from dict pickle
user_data = user_data[dict_model['variables']]


## verif variables numeriques
test_num = False
if test_num == True:
    for col in user_data.columns:
        if not pd.api.types.is_numeric_dtype(user_data[col]):
            try:
                user_data[col] = pd.to_numeric(user_data[col])
            except ValueError:
                logging.error(f"Cannot convert {col} to numeric.")


# 3. Prediction 
# infering user data with model and sotre it as a percent
prediction = dict_model['model'].predict_proba(user_data)
prediction_permilage = prediction[0][1] * 1000

# 3.1 Display results
display_results = False # pour ne pas afficher les résultats ni les stocker

if display_results == True:
    if prediction_permilage <= 333.3334:  
        category = "Vous semblez être au top!"
    elif prediction_permilage <= 666.6667:  
        category = "Vous semblez être légèrement préoccupé.."
    else:  
        category = "Et si vous pensiez à vous rapprocher du professionnel?"
    # Affichage de la catégorie
    logging.info(f"Résultat de la prédiction : {prediction_permilage:.2f}% de suggestion de voir un médecin")
    logging.info(category) 

# 4. store prédiction as txt
with open(os.path.join(src_path, 'data', 'predict.txt'), 'w') as f: 
    f.write(str(round(prediction_permilage)))
    logging.info('Prediction stored as predict.txt')
    
logging.info('------------ end predict.py ------------')  