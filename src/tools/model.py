



"""
Ce script est pour l'instant une ébacuhe de la modélisation de données que nous
allons réaliser. 

A terme il apelera la script data_cleaning puis ajustera le modèle que nous 
avons sélectionné suite au travail de data science (notebook data_expl_model)
et stockera le modèle choisi dans un fichier pickle.
"""

# 0. Imports
## 0.1. import de package
import pandas as pd
import pickle
import os
import logging
from sklearn.ensemble import AdaBoostClassifier

## 0.2 import des données
import data_cleaning

src_path = data_cleaning.nth_parent_folder(2)

## 0.3. configuration du logger
logging.basicConfig(filename=os.path.join(src_path, 'log', 'model.log'), level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
logging.info('------------ start model.py ------------')

# appel de la fonction de data cleaning
survey_mental_health_data = data_cleaning.load_n_clean_OSMI_data(warnings=False)['df_clean_signif_wo_colinear']


# 1. choix du modèle
## en regardant le notebook explo_benoit, nous avons les résultats suivants :
results_explo_dict = {'Linear SVM': {"SVC(C=1, kernel='linear')": 0.8333333333333334},
                      'Logistic Regression': {"LogisticRegression(C=0.1, penalty='l1', solver='liblinear')": 0.8214285714285714},
                      'AdaBoost': {'AdaBoostClassifier(learning_rate=0.1, n_estimators=200)': 0.8452380952380952},
                      'Random Forest': {'RandomForestClassifier(max_depth=5)': 0.8015873015873016}} 
    
""" skip this part as it is not giving any additional information from explo_benoit
# Copie profonde du dictionnaire original pour éviter les modifications inattendues
results_explo_dict_percentage = {model: {params: acc * 100 for params, acc in model_data.items()} for model, model_data in results_explo_dict.items()}

# Variables pour suivre le modèle choisi
best_model = None
best_accuracy = 0

# Affichage du nouveau dictionnaire avec les résultats en pourcentages et le symbole '%', trié par ordre ascendant
for model, model_data in sorted(results_explo_dict_percentage.items()):
    logging.info(f"{model}:")
    for params, acc in sorted(model_data.items()):
        logging.info(f"  {params}: {acc:.2f}%")
    
    # Vérifier si le modèle actuel a une meilleure précision
    if acc > best_accuracy:
        best_model = model
        best_accuracy = acc

# Afficher le modèle choisi avec les paramètres correspondants
if best_model is not None:
    logging.info(f"Modèle choisi : {best_model} avec les paramètres suivants : learning_rate=0.1, n_estimators=200")
"""

# modèle choisi : AdaBoost avec les paramètres suivants : 
## learning_rate=0.1, n_estimators=200

# 2. Ajustement du modèle
## 2.1. on crée le modèle
selected_model = AdaBoostClassifier(learning_rate=0.1, n_estimators=200)
logging.info('Model selected: AdaBoostClassifier(learning_rate=0.1, n_estimators=200)')

## 2.2. on ajuste le modèle sur les données
selected_model.fit(survey_mental_health_data.drop('treatment', axis=1), survey_mental_health_data['treatment'])
logging.info('Model fitted')

# 3. Sauvegarde du modèle
## dictionnaire pour stocker modèle et paramètres
dict_model = {'model' : selected_model,
              'variables' : survey_mental_health_data.drop('treatment', axis=1).columns}

## Save dans le fichier pickle avec wb (write binary)
with open(os.path.join(src_path, 'tools', 'stored_model.pkl'), 'wb') as file: 
    # depose le dict du modèle choisi
    pickle.dump(dict_model, file)
    logging.info('Model saved in stored_model.pkl')
    
logging.info('------------ end model.py ------------')