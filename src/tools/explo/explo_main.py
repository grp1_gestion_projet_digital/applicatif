
# # Exploration des données OSMI et travail sur la constuction d'un modèle viable

# ## import données et packages
import sys
import os
import logging
from scipy.stats import *


# Get the parent directory, add it to path and import data_cleaning
sys.path.append(os.path.dirname((os.path.dirname(os.path.realpath(__file__)))))
import data_cleaning
src_path = data_cleaning.nth_parent_folder(2)

# set up logging
logging.basicConfig(filename=os.path.join(src_path, 'log', 'model.log'), level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
logging.info('------------ start explo_main.py ------------')

# data import and split
survey_mental_health_data = data_cleaning.load_n_clean_OSMI_data(warnings=False)['df_clean_signif_wo_colinear']

X = survey_mental_health_data.drop('treatment', axis=1)
y = survey_mental_health_data['treatment']

logging.info('Data imported and split into X and y')


# ## Entraînement de modèle avec sélection paramètres par cross_validation
# ### création d'une fonction
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.model_selection import train_test_split

def grid_searchCV(X, y, model, param_grid, scoring='f1_micro', log_iter=False):
    grid_search = GridSearchCV(model, param_grid, cv=5, scoring=scoring)

    # Fit the model to the training data
    grid_search.fit(X, y)

    if log_iter == True:
        # Print all iterations of the grid search
        for i in range(len(grid_search.cv_results_['params'])):
            logging.info(f'Parameters: {grid_search.cv_results_["params"][i]}')
            logging.info(f'Mean Test Score: {grid_search.cv_results_["mean_test_score"][i]}')
            logging.info(f'Rank: {grid_search.cv_results_["rank_test_score"][i]}')
    
    return {'best_param' : grid_search.best_params_, 'score' : grid_search.best_score_}

# starting metaparameters optimization
logging.info('Starting metaparameters optimization with k-fold cross validation')

# ### SVM linéaire
svm_model = SVC()
grid_SVM_lin = {'C': [0.001, 0.1, 0.5, 1, 2,  10, 100]}

logging.info('Starting grid search for linear SVM')
score_lin_SVM = grid_searchCV(X, y, svm_model, grid_SVM_lin, 'f1_micro', log_iter=True)


# ### Lasso logistic regression
# Create the Logistic Regression model with Lasso (L1) penalty
logistic_lasso_model = LogisticRegression(penalty='l1', solver='liblinear')
grid_lasso = {'C': [0.01, 0.1, 1, 10, 11, 12, 13, 15, 20]}

logging.info('Starting grid search for lasso logistic regression')
score_lasso_logit = grid_searchCV(X, y, logistic_lasso_model, grid_lasso, 'f1_micro', log_iter=True)

# ### Adaboost
logging.info('Starting grid search for adaboost')
adaboost_model = AdaBoostClassifier()
grid_ada = {'n_estimators': [50, 100, 200],
            'learning_rate': [0.1, 0.5, 1.0]}

score_adaboost = grid_searchCV(X, y, adaboost_model, grid_ada, 'f1_micro', log_iter=True)

# ### Random Forest
logging.info('Starting grid search for random forest')
rf_model = RandomForestClassifier()
grid_rf = {'n_estimators': [50, 100, 200],
           'max_depth': [None, 5, 10, 20]}

score_rf = grid_searchCV(X, y, rf_model, grid_rf, 'f1_micro', log_iter=True)


# ## Comparaison des modèles
# ### split data and model list
logging.info('-----------------')
logging.info('Comparing performance of the models on the test set')

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1999)

# Model list 
model_name = ['Linear SVM', 'Logistic Regression', 'AdaBoost', 'Random Forest']
model_list = [SVC(C=score_lin_SVM['best_param']['C'], kernel='linear'), 
              LogisticRegression(C=score_lasso_logit['best_param']['C'], penalty='l1', solver='liblinear'), 
              AdaBoostClassifier(n_estimators=score_adaboost['best_param']['n_estimators'], 
                                 learning_rate=score_adaboost['best_param']['learning_rate']), 
              RandomForestClassifier(n_estimators=score_rf['best_param']['n_estimators'],
                                     max_depth=score_rf['best_param']['max_depth'])]

return_dict = {}
for model, name in zip(model_list, model_name):
    model.fit(X_train, y_train)
    score_test = model.score(X_test, y_test)
    logging.info(f"test of {name} on the test set")
    logging.info(f"Training accuracy: {model.score(X_train, y_train)}")
    logging.info(f"Testing accuracy: {score_test}")
    logging.info(f"Confusion matrix: \n {confusion_matrix(y_test, model.predict(X_test))}")
    return_dict[name] = {model : score_test}
    
logging.info('------------ end explo_main.py ------------')

