#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 18:06:16 2023

@author: victoria
"""

import patsy
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score


"""
Il faut d'abord modifier les noms des variables dans la bdd pcq il y a des espaces,
j'ai changé a la main vite fait pour les underscores
"""

#RLM
# Créer les matrices de conception X et Y à partir de la formule spécifiée
Y, X = patsy.dmatrices('treatment ~ Age + Gender_female + Gender_other + family_history_Yes + work_interfere_Often + work_interfere_Rarely + work_interfere_Sometimes +benefits_No + benefits_Yes + care_options_Not_sure + care_options_Yes +wellness_program_No + wellness_program_Yes + seek_help_No + seek_help_Yes +anonymity_No + anonymity_Yes + leave_Somewhat_difficult + leave_Somewhat_easy + leave_Very_difficult + leave_Very_easy + mental_health_consequence_No +mental_health_consequence_Yes + coworkers_Some_of_them + coworkers_Yes +mental_health_interview_No + mental_health_interview_Yes + mental_vs_physical_No +mental_vs_physical_Yes + obs_consequence_Yes', 
                       data=df1, 
                       return_type='dataframe')

# Diviser les données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, Y.values.ravel(), test_size=0.2, random_state=42)

# Créer et entraîner le modèle de régression logistique multinomiale
model = LogisticRegression(multi_class='multinomial', solver='lbfgs')
model.fit(X_train, y_train)

# Faire des prédictions sur l'ensemble de test
predictions = model.predict(X_test)

# Calculer la précision du modèle
accuracy = accuracy_score(y_test, predictions)
print("Précision du modèle : {:.2f}%".format(accuracy * 100))