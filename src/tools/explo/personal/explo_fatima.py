# Pour les problèmes de classification binaire,
# un SVM linéaire ou à noyau radial (RBF) peut être un bon point de départ.

# Dans ce script, nous allons créer un modèle SVM à noyau radial.
# Un modèle SVM à noyau radial est un classificateur qui utilise un noyau gaussien pour construire une frontière de décision non linéaire,
# adaptant ainsi efficacement des données complexes et de dimension élevée.

# Les bibliothèques utilisées
import sys
import os
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
import data_cleaning

# Get the parent directory, add it to path, and import data_cleaning
parent_dir = os.path.dirname(os.path.abspath(""))
sys.path.append(parent_dir)
# Importation de la base de données nettoyée sans les variables colinéaires et avec seulement les variables significatives

survey_mental_health_data = data_cleaning.load_n_clean_OSMI_data(warnings=False)['df_clean_signif_wo_colinear']
survey_mental_health_data.columns

# Séparation des données en X (variables indépendantes) et y (variable à prédire)
X = survey_mental_health_data.drop("treatment", axis=1)  # Sélection de toutes les colonnes sauf "treatment"
y = survey_mental_health_data["treatment"]

# Séparation des données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Normalisation des données
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Construction du modèle SVM à noyau radial (RBF)
model = SVC(kernel='rbf', C=1.0, gamma='scale')

# Entraînement du modèle
model.fit(X_train_scaled, y_train)

# Prédiction sur l'ensemble de test
y_pred = model.predict(X_test_scaled)

# Évaluation du modèle
accuracy = accuracy_score(y_test, y_pred)
print(f'Accuracy: {accuracy}')

#Accuracy: 0.8015873015873016 
