"""
Ce script vise à importer la base de données brute et à la transformer 
pour qu'elle puisse être utilisable dans le cadre de la construction
d'un modèle viable.

Nous avons décidé de faire une architecture avec une fonction qui réalise 
toutes les opérations de nettoyage: c'est la fonction load_n_clean_OSMI_data()
Elle renvoie 4 df stockés dans un dict (on choisit le df à utiliser selon les variables qu'on veut garder):
- df_raw: la base de données brute
- whole_df_clean_var: la base de données nettoyée avec toutes les variables
- df_clean_wo_colinear: la base de données nettoyée sans les variables colinéaires
- df_clean_signif_wo_colinear: la base de données nettoyée sans les variables colinéaires et avec seulement les variables significatives

Pour l'appliquer à un autre script ou notebook juste faire :
> parent_dir = os.path.dirname(os.path.abspath(""))
> sys.path.append(parent_dir)
> import data_cleaning2
> OSMI_data = data_cleaning.load_n_clean_OSMI_data()["df_clean_signif_wo_colinear"] (according to the variables you want to keep)

Il y a aussi une fonction qui permet de formatter les données du csv (UserData.csv) de VBA pour qu'elles soient utilisables par le modèle
"""

# 0. Imports
import pandas as pd
import os
import logging

# 1. Function definition
# Fonction pour récupérer le chemin du dossier parent
def nth_parent_folder(n: int):
    """
    Retourne le chemin du n-ieme parent dans du fichier

    Args:
        n (int): _description_

    Returns:
        str: chemin absolu vers n-ième dossier parent
    """
    # get the path of the current file
    parent_path = os.path.realpath(__file__)
    
    # go up n times in the folder hierarchy
    for i in range(n):
        parent_path = os.path.dirname(parent_path)
    
    return parent_path


# Fonction de nettoyage de la base de données
def load_n_clean_OSMI_data(warnings = True):
    """
    Retrieves the raw data as a csv and returns a dictionary with 4 dataframes:
    - df_raw: the raw data
    - whole_df_clean_var: the cleaned data with all the variables
    - df_clean_wo_colinear: the cleaned data without the colinear variables
    - df_clean_signif_wo_colinear: the cleaned data without the colinear variables and with only the significant variables
    
    Args:
        warnings (bool): if True, prints warnings about the cleaning process
    
    Returns:
        dict: a dictionary with 4 dataframes
    """
    # 1. import data
    df = pd.read_csv(os.path.join(nth_parent_folder(2), 'data', 'raw_data.csv'))

    # 2. data cleaning step
    # 2.0 keeping a raw copy of the data
    df_raw = df.copy()

    # 2.1. dropping the useless columns
    df = df.drop(['Timestamp', 'comments', 'state', 'Country'], axis = 1)
    df_initial = df.copy()

    # 2.2. cleaning the gender variable
    # encoding the gender variable into 3 categories (male, female and non_binary)
    df["Gender"] = df["Gender"].replace({'women' : 'female', 
                                        'something kinda male?' : 'male',
                                        'queer/she/they' : 'non_binary', 
                                        'queer': 'non_binary',
                                        'p' : 'non_binary',
                                        'ostensibly male, unsure what that really means' : 'male',
                                        'non-binary' : 'non_binary',
                                        'msle' : 'male', 
                                        'male leaning androgynous' : 'male',
                                        'maile': 'male',
                                        'm' : 'male',
                                        'fluid' : 'non_binary', 
                                        'non-binary' : 'non_binary',
                                        'Man' : 'male',
                                        'Malr' : 'male', 
                                        'Nah' : 'non_binary',
                                        'Male-ish' : 'male', 
                                        'Male (CIS)' : 'male',
                                        'maile': 'male', 
                                        'M' : 'male', 
                                        'Mail' : 'male', 
                                        'Guy (-ish) ^_^' : 'male',
                                        'Cis Male' : 'male', 
                                        'Cis Man': 'male', 
                                        'Mal' : 'male', 
                                        'Male' : 'male', 
                                        'Make' : 'male', 
                                        'A little about you' : 'non_binary',
                                        'Agender' : 'non_binary', 
                                        'All' : 'non_binary', 
                                        'Androgyne' : 'non_binary', 
                                        'Cis Female' : 'female',
                                        'Enby': 'non_binary', 
                                        'F' : 'female', 
                                        'Femake' : 'female',
                                        'Female (cis)' : 'female', 
                                        'Genderqueer' : 'non_binary', 
                                        'Female (trans)': 'female',
                                        'Trans woman' : 'female', 
                                        'Trans-female' : 'female', 
                                        'Woman' : 'female', 
                                        'cis-female/femme' : 'female',
                                        'f': 'female', 
                                        'femail' : 'female',
                                        'women' : 'female',
                                        'Neuter' : 'non_binary', 
                                        'Female' : 'female',
                                        'Male' : 'male', 
                                        'cis male' : 'male', 
                                        'woman' : 'female',
                                        'Female ' : 'female', 
                                        'Male ' : 'male'})  


    # 2.3 string obs cleaning (this is done to have nice dummies later on)
    # replacing all the spaces by underscores in the string obs
    df = df.replace(' ', '_', regex=True)
    # replacing all the ' by "" in the string obs
    df = df.replace("'", "", regex=True)
    # lowercasing all obs
    df = df.applymap(lambda s: s.lower() if type(s) == str else s)


    # 2.4. binarization and categorization of the variables
    # observing the data, we made a list of:
    #   - the variables that we want to binarize or categorize 
    #   - the one that had a lot of NaN and of the one that had a lot of idk answers
    var_list_binar_noNaN = ["family_history", "treatment", "remote_work", "tech_company", "obs_consequence"]
    var_list_binar_NaN = ["self_employed"]
    var_list_categ_high_nb_NaN = ["work_interfere"]
    var_list_categ_high_nb_idk = ["benefits", "care_options", "wellness_program", "seek_help", "anonymity", "leave", "mental_health_consequence", "phys_health_consequence", "mental_health_interview", "phys_health_interview", "mental_vs_physical"]
    var_list_categ = ['Gender', 'coworkers', 'no_employees', 'supervisor']

    # mapping for the binarization
    yes_no_mapping = {'yes': 1, 'no': 0}

    # y/n into dummies for the variables with no NaN
    for column in var_list_binar_noNaN:
        df[column] = df[column].map(yes_no_mapping)
        
    # y/n into dummies for the variables with NaN and replace the NaN by 'no' 
    # (printing a message to know which variables are concerned)
    for column in var_list_binar_NaN:
        if warnings == True: logging.warning(f"Replacing NaN in {column} by 'no'")
        df[column] = df[column].fillna('no')
        df[column] = df[column].map(yes_no_mapping)

    # categ into dummies for the variables with a lot of NaN and replace the NaN by 'unknown'  
    # (printing a message to know which variables are concerned)
    for column in var_list_categ_high_nb_NaN:
        if warnings == True: logging.warning(f"Replacing NaN in {column} by 'unknown'")
        df[column] = df[column].fillna('unknown')
    df = pd.get_dummies(df, columns=var_list_categ_high_nb_NaN)

    # categ into dummies for the variables with a lot of idk answers 
    # (printing a message to know which variables are concerned)
    for column in var_list_categ_high_nb_idk:
        if warnings == True: logging.warning(f"Lots of idk answers in {column}, care bout that")
    df = pd.get_dummies(df, columns=var_list_categ_high_nb_idk)

    # categ into dummies in the case of no issue
    df = pd.get_dummies(df, columns=var_list_categ)


    # 2.5 finally lowercasing all the columns names for the saké of simplicity
    df.columns = df.columns.str.lower()
    df_initial.columns = df_initial.columns.str.lower()


    # 2.6. dealing with colinearity
    # selection of the variables that are not colinear
    # gettin the column list of the initial df and the final df
    initial_var_list = sorted(list(df_initial.columns))
    final_var_list = sorted(list(df.columns))

    # making a nested grouping dummies by main variables
    nested_var_list = [[j for j in final_var_list if i in j] for i in initial_var_list]

    # droping the first dummy of each nested list and making it a simple list
    var_list_wo_colinear = [item for sublist in [i if len(i)==1 else i[1:len(i)] for i in nested_var_list] for item in sublist]


    # 2.7. selecting the significant variables
    # selection of significant variables based on a chi2 test (is the var independant from treatment)
    significant_variables = ["gender",
                            "family_history",
                            "work_interfere",
                            "benefits",
                            "care_options",
                            "wellness_program",
                            "seek_help",
                            "anonymity",
                            "treatment",
                            "leave",
                            "mental_health_consequence",
                            "coworkers",
                            "mental_health_interview",
                            "mental_vs_physical",
                            "obs_consequence"] 
    var_list_signif_wo_colinear = [i for i in var_list_wo_colinear if any(pattern in i for pattern in significant_variables)]
    
    # 3. dictionary to return
    output_dict = {"df_raw" : df_raw,
                   "whole_df_clean": df[final_var_list],
                   "df_clean_wo_colinear": df[var_list_wo_colinear],
                   "df_clean_signif_wo_colinear": df[var_list_signif_wo_colinear]}
    
    return(output_dict)

def format_user_data(userdata: pd.DataFrame):
    """Returns the formatted user data with the whole list of dummyvariables

    Args:
        userdata (pd.DataFrame): user data coming from csv and VBA
    """
    # dict to translate the french answers into english - to be polished ...................................................................................................
    userdata.columns = userdata.columns.str.lower()
    dict_fr_en = {"Oui" : "yes",
                  "Non" : "no",
                  "Homme" : "male",
                  "Femme" : "female",
                  "Autre" : "non_binary",
                  "Je ne sais pas" : "unknown",
                  "Rarement" : "rarely",
                  "Souvent" : "often",
                  "Parfois" : "sometimes",
                  "Jamais" : "never",
                  "Très facile" : "very_easy",
                  "Assez facile" : "somewhat_easy",
                  "Assez difficile" : "somewhat_difficult",
                  "Très difficile" : "very_difficult",
                  "Certains" : "some_of_them",
                  "Peut-etre" : "maybe",
                  "Pas sur" : "dont_know"}
    userdata = userdata.replace(dict_fr_en)
    
    # remplir les dummys manquantes avec des 0
    entire_variables = ['anonymity_dont_know', 'anonymity_no', 'anonymity_yes', 
                        'benefits_dont_know', 'benefits_no', 'benefits_yes', 'care_options_no',
                        'care_options_not_sure', 'care_options_yes', 'coworkers_no',
                        'coworkers_some_of_them', 'coworkers_yes', 'family_history',
                        'gender_female', 'gender_male', 'gender_non_binary', 'leave_dont_know',
                        'leave_somewhat_difficult', 'leave_somewhat_easy',
                        'leave_very_difficult', 'leave_very_easy',
                        'mental_health_consequence_maybe', 'mental_health_consequence_no',
                        'mental_health_consequence_yes', 'mental_health_interview_maybe',
                        'mental_health_interview_no', 'mental_health_interview_yes',
                        'mental_vs_physical_dont_know', 'mental_vs_physical_no',
                        'mental_vs_physical_yes', 'no_employees_1-5', 'no_employees_100-500',
                        'no_employees_26-100', 'no_employees_500-1000', 'no_employees_6-25',
                        'no_employees_more_than_1000', 'obs_consequence',
                        'phys_health_consequence_maybe', 'phys_health_consequence_no',
                        'phys_health_consequence_yes', 'phys_health_interview_maybe',
                        'phys_health_interview_no', 'phys_health_interview_yes', 'remote_work',
                        'seek_help_dont_know', 'seek_help_no', 'seek_help_yes', 'self_employed',
                        'supervisor_no', 'supervisor_some_of_them', 'supervisor_yes',
                        'tech_company', 'treatment', 'wellness_program_dont_know',
                        'wellness_program_no', 'wellness_program_yes', 'work_interfere_never',
                        'work_interfere_often', 'work_interfere_rarely',
                        'work_interfere_sometimes', 'work_interfere_unknown']
    
    userdata = pd.get_dummies(userdata)
    for col in entire_variables:
        if col not in userdata.columns:
            userdata[col] = 0
    
    return(userdata)


# 2. Test fonctionnement général
if __name__ == "__main__":       
    print(nth_parent_folder(2))                                                     
    print(load_n_clean_OSMI_data()["df_clean_wo_colinear"].columns)
    print(load_n_clean_OSMI_data()["df_clean_signif_wo_colinear"].columns)
    print(load_n_clean_OSMI_data()["whole_df_clean"].columns)
    print(format_user_data(pd.DataFrame({"seek_help" : ["Oui"], "family_history" : ["Non"]})))

test = load_n_clean_OSMI_data()["whole_df_clean"]

effectifs_treatment = test['treatment'].value_counts()
print("Effectifs pour 'treatment':\n", effectifs_treatment)  

df_cleaned = test.dropna()
