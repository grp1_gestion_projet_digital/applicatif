#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Ce test unitaire vise à verifier le fonctionnement du model qui est stocké sur pickle 
P.S. d'abord il faut ajouter une fonction dans model.py : 

# 0.0 Unittest
def train_and_save_model(pickle_path):
    pass

"""
import unittest
from model import train_and_save_model
from pathlib import Path
import os

class TestTrainAndSaveModel(unittest.TestCase):

    def test_train_and_save_model(self):
        # Créez un chemin pour le fichier pickle de test
        test_pickle_path = os.path.join(Path(__file__).parent, 'stored_model.pkl')

        # Appelez la fonction de formation et de sauvegarde du modèle
        train_and_save_model(test_pickle_path)

        # Vérifiez si le fichier pickle a été créé
        self.assertTrue(os.path.isfile(test_pickle_path))

        # Vous pouvez ajouter d'autres assertions si nécessaire

if __name__ == '__main__':
    unittest.main()

