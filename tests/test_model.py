
"""
Ce test vise a verifier le fonctionnement du model dans un pickle

"""

import unittest
from model import train_and_save_model
from sklearn.ensemble import AdaBoostClassifier
import pandas as pd
import os
import pickle
import data_cleaning

class TestYourCode(unittest.TestCase):

    def setUp(self):
        self.pickle_path = 'stored_model.pkl'
        self.data = data_cleaning.load_n_clean_OSMI_data(warnings=False)['df_clean_signif_wo_colinear']

    def test_train_and_save_model(self):
        # Arrange & Act
        train_and_save_model(self.pickle_path)

        # Assert
        self.assertTrue(os.path.exists(self.pickle_path), "Le fichier pickle n'a pas été créé.")

        # Vérifiez que le fichier pickle peut être chargé correctement
        with open(self.pickle_path, 'rb') as file:
            model_dict = pickle.load(file)
            self.assertIn('model', model_dict)
            self.assertIn('variables', model_dict)

    def test_adaboost_model_performance(self):
        # Arrange
        X = self.data.drop('treatment', axis=1)
        y = self.data['treatment']

        # Act
        model = AdaBoostClassifier(learning_rate=0.1, n_estimators=200)
        model.fit(X, y)

        # Assert
        score = model.score(X, y)
        self.assertGreater(score, 0.7, "Le modèle AdaBoost n'a pas atteint la performance attendue.")

    def test_model_integrity_after_loading(self):
        # Arrange & Act
        train_and_save_model(self.pickle_path)

        # Assert
        with open(self.pickle_path, 'rb') as file:
            model_dict = pickle.load(file)
            model = model_dict['model']
            variables = model_dict['variables']
            self.assertIsInstance(model, AdaBoostClassifier)
            self.assertEqual(len(variables), len(self.data.columns) - 1)  # Exclure la colonne 'treatment'

    def tearDown(self):
        # Nettoyage
        if os.path.exists(self.pickle_path):
            os.remove(self.pickle_path)

if __name__ == '__main__':
    unittest.main()
