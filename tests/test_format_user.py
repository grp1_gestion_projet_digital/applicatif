#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 15 10:32:51 2023

@author: remy
"""

import unittest
import pandas as pd
from user_data_formatting import format_user_data

class TestFormatUserData(unittest.TestCase):
    def setUp(self):
        self.test_data = pd.DataFrame({
            'seek_help': ['Oui', 'Non', None],
            'family_history': ['Non', 'Oui', ''],
        })

    def test_format_user_data(self):
        # Colonnes attendues
        expected_columns = [
        'seek_help_yes', 'family_history_no', 'anonymity_dont_know', 'anonymity_no', 'anonymity_yes',
        'benefits_dont_know', 'benefits_no', 'benefits_yes', 'care_options_no', 'care_options_not_sure',
        'care_options_yes', 'coworkers_no', 'coworkers_some_of_them', 'coworkers_yes', 'family_history',
        'gender_female', 'gender_male', 'gender_non_binary', 'leave_dont_know', 'leave_somewhat_difficult',
        'leave_somewhat_easy', 'leave_very_difficult', 'leave_very_easy', 'mental_health_consequence_maybe',
        'mental_health_consequence_no', 'mental_health_consequence_yes', 'mental_health_interview_maybe',
        'mental_health_interview_no', 'mental_health_interview_yes', 'mental_vs_physical_dont_know',
        'mental_vs_physical_no', 'mental_vs_physical_yes', 'no_employees_1-5', 'no_employees_100-500',
        'no_employees_26-100', 'no_employees_500-1000', 'no_employees_6-25', 'no_employees_more_than_1000',
        'obs_consequence', 'phys_health_consequence_maybe', 'phys_health_consequence_no',
        'phys_health_consequence_yes', 'phys_health_interview_maybe', 'phys_health_interview_no',
        'phys_health_interview_yes', 'remote_work', 'seek_help_dont_know', 'seek_help_no', 'self_employed',
        'supervisor_no', 'supervisor_some_of_them', 'supervisor_yes', 'tech_company', 'treatment',
        'wellness_program_dont_know', 'wellness_program_no', 'wellness_program_yes', 'work_interfere_never',
        'work_interfere_often', 'work_interfere_rarely', 'work_interfere_sometimes', 'work_interfere_unknown'
]

        formatted_data = format_user_data(self.test_data)

        # Vérifier que les colonnes attendues sont dans le DataFrame formaté
        for col in expected_columns:
            self.assertTrue(col in formatted_data.columns, f"Colonne manquante : {col}")

        # Vérifier les traductions
        self.assertEqual(formatted_data.loc[0, 'seek_help_yes'], 1)
        self.assertEqual(formatted_data.loc[1, 'seek_help_no'], 1)

        # Vérifier la gestion des valeurs manquantes
        self.assertEqual(formatted_data.loc[2, 'seek_help_yes'], 0)
        self.assertEqual(formatted_data.loc[2, 'seek_help_no'], 0)

        # Vérifier les valeurs par défaut pour les colonnes manquantes
        for col in expected_columns:
            if col not in self.test_data.columns:
                self.assertTrue((formatted_data[col] == 0).all())

if __name__ == '__main__':
    unittest.main()
