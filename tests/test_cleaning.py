#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Ce test unitaire vise à verifier les variables dans la data_cleaning

"""

import unittest
import pandas as pd
from data_cleaning import nth_parent_folder, load_n_clean_OSMI_data

class TestDataCleaning(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Cette méthode sera exécutée une seule fois avant tous les tests
        cls.data_path = nth_parent_folder(2) + '/data/raw_data.csv'

    def test_nth_parent_folder(self):
        # Test pour s'assurer que nth_parent_folder renvoie le bon chemin
        expected_path = 'le_chemin_attendu'  # Remplacez ceci par le chemin attendu
        self.assertEqual(nth_parent_folder(2), expected_path)

    def test_load_n_clean_OSMI_data_import(self):
        # Test pour vérifier l'importation des données
        data_dict = load_n_clean_OSMI_data()
        self.assertIsInstance(data_dict, dict)
        self.assertIn('df_raw', data_dict)
        self.assertIn('whole_df_clean', data_dict)

    def test_data_cleaning_steps(self):
        # Test pour vérifier les étapes de nettoyage des données
        data_dict = load_n_clean_OSMI_data()
        # Exemple : vérifier si certaines colonnes ont été supprimées
        self.assertNotIn('Timestamp', data_dict['whole_df_clean'].columns)

    def test_handling_missing_values(self):
        # Test pour vérifier la gestion des valeurs manquantes
        data_dict = load_n_clean_OSMI_data()
        # Exemple : vérifier que les NaN dans 'self_employed' sont bien traités
        self.assertFalse(data_dict['whole_df_clean']['self_employed'].isnull().any())

    def test_colinearity_handling(self):
        # Test pour vérifier la gestion de la colinéarité
        data_dict = load_n_clean_OSMI_data()
        # Exemple : vérifier la suppression de certaines variables dummy
        self.assertNotIn('dummy_var_to_remove', data_dict['df_clean_wo_colinear'].columns)

    def test_significant_variable_selection(self):
        # Test pour vérifier la sélection des variables significatives
        data_dict = load_n_clean_OSMI_data()
        # Exemple : vérifier la présence d'une variable significative
        self.assertIn('significant_var', data_dict['df_clean_signif_wo_colinear'].columns)

    @classmethod
    def tearDownClass(cls):
        # Cette méthode sera exécutée une fois après tous les tests
        pass

if __name__ == '__main__':
    unittest.main()
