import unittest
import numpy as np
from sklearn.datasets import make_classification
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from explo_benoit import grid_searchCV  
#j'ai exporté le fichier explo_benoit en .py

class TestGridSearchCV(unittest.TestCase):
    def setUp(self):
        # Generate synthetic data for testing
        X, y = make_classification(n_samples=100, n_features=20, n_classes=2, random_state=42)
        self.X = X
        self.y = y

    def test_grid_searchCV(self):
        # Define the SVM model with a radial kernel
        svm_model = SVC(kernel='rbf')

        # Define the parameter grid for the grid search
        param_grid = {'C': [0.1, 1, 10], 'gamma': [0.01, 0.1, 1]}

        # Call the grid_searchCV function
        result = grid_searchCV(self.X, self.y, svm_model, param_grid, scoring='f1_micro', print_iter=True)

        # Perform assertions
        self.assertIsInstance(result, dict)
        self.assertIn('best_param', result)
        self.assertIn('score', result)

if __name__ == '__main__':
    unittest.main()
