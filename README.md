# Application - Prédiction risque santé mentale

Fatima AAGOUR - Rémy ANGELIER - Viktoriia KABAKOVA - Amélie LACROIX - Benoit MARION

![app](https://gitlab-mi.univ-reims.fr/grp1_gestion_projet_digital/applicatif/-/wikis/uploads/fffb0d1510435072a00a740dccf2cf5f/app.png)


## Présentation succincte

Dans le monde professionnel d'aujourd'hui, la santé mentale au travail est devenue une préoccupation majeure. Elle impacte non seulement le bien-être des individus, mais également la productivité, l'efficacité et, en fin de compte, la rentabilité de l'entreprise. Nous proposons un outil qui, en se basant sur des informations fournies par l'employé, est capable de déterminer si un individu est susceptible de nécessiter un soutien ou un accompagnement en matière de santé mentale.


## *Requirements*

- OS Microsoft (version 8+)
- Python (v 3.11 de préférence)
- Python packages : `pandas`, `numpy`, `scipy`  et `sklearn`
- Microsoft Excel 2010+ avec les macros activées


## Fichiers du dépôt

- un dossier `src` incluant les fichiers pour faire fonctionner l'application incluant :
    - un dossier `ui` avecles ressouces necessaires (images...)
    - dossier `data` stockant les données collectées afin de modéliser le risque pour la santé mentale et les fichiers temporaires pour réaliser la prédiction (supprimés après utilisation)
    - un dossier `tools` contenant les scripts python de notre programme (nettoyage des données, modélisation, prédiction et nettoyage de l'environnement après utilisation) et un fichier `.plk` (tmtc) avec notre modèle entraîné
- un dossier `doc` avec quelques documents intéressants pour comprendre plus précisément notre projet (rapport et explications détaillées sur le produit, notice...)
- un dossier `test` contenant des tests unitaires afin de s'assurer du bon fonctionnement de nos programmes
- ce `README.md`
- et surtout `app.xlsm` qui est le fichier contenant l'ensemble de l'application


## Fonctionnement 
### théorique :

1) Partage d'informations : L'employé répond à quelques questions simples et discrètes via une interface conviviale. C'est comme remplir un petit questionnaire.
2) Analyse rapide : Une fois le questionnaire rempli, notre détecteur examine les réponses. Il utilise des méthodes intelligentes pour évaluer si un employé pourrait bénéficier d'un soutien en matière de santé mentale.
3) Recommandations : Sur la base de l'analyse, l'outil fournit des indications. Il ne s'agit pas d'un diagnostic médical, mais d'une suggestion que l'entreprise peut utiliser pour offrir un soutien ou des ressources adaptées à l'employé.

### en pratique
0) avoir les dépendances installées : 
    - Excel avec onglet développeur activé
    - Python et packages python avec `pip install pandas numpy scipy sklearn` ou `conda install pandas numpy scipy sklearn` (si environnement python géré par anaconda)
1) activer dans les propriétés du fichier excel `app.xlsm` les macros (*cf* ce [tuto](https://exceloffthegrid.com/unblock-macros-downloaded-from-the-internet/)).
2) ouvrir le fichier excel et double-clicker sur le bouton visible
3) remplir le formulaire
4) attendre quelques secondes
5) obtenir un visuel de prédiction et de conseil adapté

## Protection des données personnelles
La question des données personnelles est un enjeu central dans un environnement tel que la santé au travail. Il est pour nous essentiel d'assurer la confidentialité des données de nos utilisateurs. Pour ce faire, notre application inclura différents systèmes pour éviter toute fuite de données :

- Notre modèle est entraîné sur des données anonymes
- Aucune question permettant d'identifier directement le répondant n'est posée
- Une fois la prédiction obtenue, toute les données personnelles sont supprimées
- Aucun fichier log avec des données individuelles n'est conservé par notre application (deux fichiers log : `model.log` qui permet de voir comment est contruit le modèle prédictif, et `use.log` qui permet de lister les utilisation des scripts `predict.py` et `clearing_user_data.py`).
- L'application tourne en local sans aucune connexion à internet.


